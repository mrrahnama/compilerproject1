// This file has been prepared for Doxygen automatic documentation generation.
/*! \file ********************************************************************
*
* \brief  Header file for application common functions.
*
* This file contains the prototypes for common functions needed
* by the Remote Keyless Entry application.
* The implementation is found in the common.c file.
*
* - File:              common.h
* - Compiler:          IAR EWAVR 4.11B
* - Supported devices: ATtiny45/85
* - AppNote:           AVR411 - Secure Rolling Code Algorithm
*                      for Wireless Link
*
* \author              Atmel Corporation: http://www.atmel.com \n
*                      Support email: avr@atmel.com
*
* $Name:  $
* $Revision: 1193 $
* $Date: 2006-10-31 14:21:08 +0100 (ti, 31 okt 2006) $
*
* Copyright (c) 2006, Atmel Corporation All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* 3. The name of ATMEL may not be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY ATMEL ``AS IS'' AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE EXPRESSLY AND
* SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT,
* INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
#ifndef COMMON_H
#define COMMON_H

//#include <ioavr.h>
//#include <inavr.h>
#include<stm8s_flash.h>


//// Typedefs from the C99 standard.
//typedef signed char int8_t;
//typedef unsigned char uint8_t;
//typedef short int16_t;
//typedef unsigned short uint16_t;
//typedef long int32_t;
//typedef unsigned long uint32_t;

typedef uint8_t byte; //!< Handy typedef. Very readable.



//! Copy 'count' bytes from 'source' buffer to 'destination' buffer in SRAM.
void copyBytes( byte * destination, const byte * source, byte count);
//! XOR 'count' bytes from 'constant' buffer in EEPROM into 'bytes' buffer in SRAM.
void addConstantFromEEPROM( byte * bytes,
		const byte __eeprom * constant, byte count );
//! Copy 'count' bytes from 'source' buffer in SRAM to 'destination' buffer in EEPROM.
void copyBytesToEEPROM( byte __eeprom * destination,
		const byte * source, byte count );
//! Copy 'count' bytes from 'source' buffer in EEPROM to 'destination' buffer in SRAM.
void copyBytesFromEEPROM( byte * destination,
		const byte __eeprom * source, byte count );

//TIM2_GetCounter()

#endif

