// This file has been prepared for Doxygen automatic documentation generation.
/*! \file ********************************************************************
*
* \brief  Source file for CMAC public and support functions.
*
* This file contains the function implementation for the CMAC
* generation functions. Refer to the cmac.h file for more details.
*
* - File:              cmac.c
* - Compiler:          IAR EWAVR 4.11B
* - Supported devices: ATtiny45/85
* - AppNote:           AVR411 - Secure Rolling Code Algorithm
*                      for Wireless Link
*
* \author              Atmel Corporation: http://www.atmel.com \n
*                      Support email: avr@atmel.com
*
* $Name:  $
* $Revision: 1193 $
* $Date: 2006-10-31 14:21:08 +0100 (ti, 31 okt 2006) $
*
* Copyright (c) 2006, Atmel Corporation All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* 3. The name of ATMEL may not be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY ATMEL ``AS IS'' AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE EXPRESSLY AND
* SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT,
* INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
#include "cmac.h"
#include "aes.h"
#include "memory.h"
#include "common.h"
#include "config.h"



//! AES-CMAC subkey storage in EEPROM.
__no_init byte __eeprom CMACSubkey[ CMAC_SUBKEY_SIZE ];



void calcCMACSubkey( byte * cryptoBlock )
{
	byte blockSize;
	byte * blockPtr;
	byte overflow;

	// Fill crypto block with all zeros.
	blockSize = BLOCK_SIZE;
	blockPtr = cryptoBlock;
	do {
		*blockPtr++ = 0;
	} while( --blockSize );

	// Encrypt zeros with secret key (assuming
	// key schedule has been created with secret key)
	cipherLookup( cryptoBlock );

	// Multiply by 2 modulo (0b1^120 cat 0b10000111).
	blockSize = BLOCK_SIZE - 1;
	blockPtr = cryptoBlock;
	overflow = blockPtr[0] & 0x80;
	do {
		blockPtr[0] = (blockPtr[0] << 1) | (blockPtr[1] >> 7);
		++blockPtr;
	} while( --blockSize );
	blockPtr[0] <<= 1;
	if( overflow ) {
		blockPtr[0] ^= 0x87;
	}

#if MESSAGE_SIZE_WO_MAC < BLOCK_SIZE
	// Multiply by 2 modulo (0b1^120 cat 0b10000111) again.
	blockSize = BLOCK_SIZE - 1;
	blockPtr = cryptoBlock;
	overflow = blockPtr[0] & 0x80;
	do {
		blockPtr[0] = (blockPtr[0] << 1) | (blockPtr[1] >> 7);
		++blockPtr;
	} while( --blockSize );
	blockPtr[0] <<= 1;
	if( overflow ) {
		blockPtr[0] ^= 0x87;
	}
#endif

	copyBytesToEEPROM( CMACSubkey, cryptoBlock, BLOCK_SIZE );
}



void calcCMAC( const byte * message, byte * MACStorage )
{
	// Copy message data to MACStorage, which should be BLOCK_SIZE.
	copyBytes( MACStorage, message, MESSAGE_SIZE_WO_MAC );

#if MESSAGE_SIZE_WO_MAC < BLOCK_SIZE
	// Pad message before calculating CMAC.
  #if MESSAGE_SIZE_WO_MAC + 1 < BLOCK_SIZE
	byte pos = MESSAGE_SIZE_WO_MAC + 1;
	do {
		MACStorage[ pos ] = 0x00;
	} while( ++pos < BLOCK_SIZE );
  #endif
	MACStorage[ MESSAGE_SIZE_WO_MAC ] = 0x80;
#endif

	// Add (XOR) subkey.
	addConstantFromEEPROM( MACStorage, CMACSubkey, BLOCK_SIZE );

	// Encrypt, assuming secret key schedule is precalculated.
	cipherLookup( MACStorage );
}

