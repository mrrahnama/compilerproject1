// This file has been prepared for Doxygen automatic documentation generation.
/*! \file ********************************************************************
*
* \brief  Header file for application configuration options.
*
* This file contains the configuration options for the Remote Keyless Entry
* application. First, there are a set of user configurable options, then
* a lot of calculations based on the user configuration. Do not change the
* calculations unless you really know what you are doing.
*
* - File:              config.h
* - Compiler:          IAR EWAVR 4.11B
* - Supported devices: ATtiny45/85
* - AppNote:           AVR411 - Secure Rolling Code Algorithm
*                      for Wireless Link
*
* \author              Atmel Corporation: http://www.atmel.com \n
*                      Support email: avr@atmel.com
*
* $Name:  $
* $Revision: 1269 $
* $Date: 2006-12-06 12:37:09 +0100 (on, 06 des 2006) $
*
* Copyright (c) 2006, Atmel Corporation All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* 3. The name of ATMEL may not be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY ATMEL ``AS IS'' AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE EXPRESSLY AND
* SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT,
* INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
#ifndef CONFIG_H
#define CONFIG_H


#define ASK //!< Uncomment to use Amplitude Shift Keying.
//#define FSK //!< Uncomment to use Frequency Shift Keying.

//! Bits per second. Note: Manchester uses to bits per data bit.
//#define BPS_19200
#define BPS_9600
//#define BPS_4800
//#define BPS_2400


//! Disable all use of watchdog timer except in error loop.
#define NOWDT

//! Disable BOD Reset Flag checking during startup, to avoid lock-up during debugging.
#define NOBOD

//! Invert manchester output polarity, ie. 0 = 1->0, 1 = 0->1
#define INVERTED_MANCHESTER
//! Use UART asynchronous transmission instead of Manchester.
//#define USE_UART

#define KEY_BITS 128 //!< Use AES128
//#define KEY_BITS 192 //!< Use AES192
//#define KEY_BITS 256 //!< Use AES256

//! How many BLOCK_SIZE blocks of precalculated key schedule in SRAM (rest in EEPROM).
#define SCHEDULE_SPLIT_BLOCKS 8

//! Number of preburst bytes for waking up the receiver. Empirical value.
#define PREBURST_BYTES 16

//! Size in bytes of serial number (1, 2 or 4).
#define SERIAL_NO_BYTES 4

//! Size in bytes of command code (1, 2 or 4).
#define COMMAND_CODE_BYTES 1

//! Size in bytes of sequential counter (1, 2 or 4).
#define SEQ_COUNTER_BYTES 4

//! Size in bytes of MAC (must be less than or equal to AES BLOCK_SIZE).
#define MAC_BYTES 4

//! CRC generator polynomial.
#define CRC_POLY 0x8005 // CRC-16 standard.
//#define POLY 0x1021 // CRC-CCITT standard.

//! Maximum number of watchdog resets before entering error loop.
#define MAX_WATCHDOG_RESETS 3

//! Bitmask to use when masking out unwanted inputs when reading buttons.
#define BUTTON_MASK ((1<<PB1) | (1<<PB2))

//! Bitpattern indicating the button combination to use for teach mode.
#define TEACH_COMMAND ((1<<PB1) | (1<<PB2))





/* --- Calculated parameters follow, do not change --- */

//! AES state block size in number of bytes.
#define BLOCK_SIZE 16
//! Subkey used for calculating AES-CMAC value.
#define CMAC_SUBKEY_SIZE BLOCK_SIZE

// Calculate some info based on key bit count.
#if KEY_BITS == 128
  #define ROUNDS 10 //!< Number of rounds/iterations in algorithm.
  #define KEY_SIZE 16 //!< Key length in number of bytes.
#elif KEY_BITS == 192
  #define ROUNDS 12 //!< Number of rounds/iterations in algorithm.
  #define KEY_SIZE 24 //!< Key length in number of bytes.
#elif KEY_BITS == 256
  #define ROUNDS 14 //!< Number of rounds/iterations in algorithm.
  #define KEY_SIZE 32 //!< Key length in number of bytes.
#else
  #error Key must be 128, 192 or 256 bits!
#endif

//! Number of BLOCK_SIZE blocks in precalculated key schedule.
#define SCHEDULE_SIZE_BLOCKS (ROUNDS+1)
//! Number of bytes in precalculated key schedule.
#define SCHEDULE_SIZE (SCHEDULE_SIZE_BLOCKS*BLOCK_SIZE)
//! How many bytes before splitting schedule to secondary memory.
#define SCHEDULE_SPLIT (SCHEDULE_SPLIT_BLOCKS*BLOCK_SIZE)
//! How many BLOCK_SIZE blocks of eschedule in secondary memory.
#define SCHEDULE_EXTRA_BLOCKS (SCHEDULE_SIZE_BLOCKS-SCHEDULE_SPLIT_BLOCKS)
//! How many bytes of schedule in secondary memory.
#define SCHEDULE_EXTRA (SCHEDULE_EXTRA_BLOCKS*BLOCK_SIZE)

// Check validity of schedule split configuration.
#if SCHEDULE_EXTRA < 0
  #error Split cannot exceed schedule length
#endif

//! Message payload size in byte, excluding the MAC.
#define MESSAGE_SIZE_WO_MAC (SERIAL_NO_BYTES+COMMAND_CODE_BYTES+SERIAL_NO_BYTES)

// Check that message payload does not exceed AES block size.
#if MESSAGE_SIZE_WO_MAC > BLOCK_SIZE
  #error Message payload exceeds AES block size.
#endif

//! Total message size in bytes, including the MAC.
#define MESSAGE_SIZE_W_MAC (MESSAGE_SIZE_WO_MAC+MAC_BYTES)

// Prepare type and max value for serial number.
#if SERIAL_NO_BYTES == 1
  #define SERIAL_NO_TYPE byte
  #define SERIAL_NO_MAX 0xff
#elif SERIAL_NO_BYTES == 2
  #define SERIAL_NO_TYPE uint16_t
  #define SERIAL_NO_MAX 0xffff
#elif SERIAL_NO_BYTES == 4
  #define SERIAL_NO_TYPE uint32_t
  #define SERIAL_NO_MAX 0xffffffff
#else
  #error Invalid serial number size, must be 1, 2 or 4 bytes.
#endif

// Prepare type and max value for command code.
#if COMMAND_CODE_BYTES == 1
  #define COMMAND_CODE_TYPE byte
  #define COMMAND_CODE_MAX 0xff
#elif COMMAND_CODE_BYTES == 2
  #define COMMAND_CODE_TYPE uint16_t
  #define COMMAND_CODE_MAX 0xffff
#elif COMMAND_CODE_BYTES == 4
  #define COMMAND_CODE_TYPE uint32_t
  #define COMMAND_CODE_MAX 0xffffffff
#else
  #error Invalid command code size, must be 1, 2 or 4 bytes.
#endif

// Prepare type and max value for sequential counter.
#if SEQ_COUNTER_BYTES == 1
  #define SEQ_COUNTER_TYPE byte
  #define SEQ_COUNTER_MAX 0xff
#elif SEQ_COUNTER_BYTES == 2
  #define SEQ_COUNTER_TYPE uint16_t
  #define SEQ_COUNTER_MAX 0xffff
#elif SEQ_COUNTER_BYTES == 4
  #define SEQ_COUNTER_TYPE uint32_t
  #define SEQ_COUNTER_MAX 0xffffffff
#else
  #error Invalid sequential counter size, must be 1, 2 or 4 bytes.
#endif

//! EEPROM address of serial number.
#define SERIAL_NO_ADDRESS 0

//! EEPROM address of sequential counter.
#define SEQ_COUNTER_ADDRESS (SERIAL_NO_ADDRESS+SERIAL_NO_BYTES)

//! EEPROM address of secret key.
#define SECRET_KEY_ADDRESS (SEQ_COUNTER_ADDRESS+SEQ_COUNTER_BYTES)

//! EEPROM address of shared key.
#define SHARED_KEY_ADDRESS (SECRET_KEY_ADDRESS+KEY_SIZE)



#endif

