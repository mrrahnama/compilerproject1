// This file has been prepared for Doxygen automatic documentation generation.
/*! \file ********************************************************************
*
* \brief  Source file for communications public and support functions.
*
* This file contains the function implementation for the communications
* control functions. Refer to the comms.h file for more details.
*
* - File:              comms.c
* - Compiler:          IAR EWAVR 4.11B
* - Supported devices: ATtiny45/85
* - AppNote:           AVR411 - Secure Rolling Code Algorithm
*                      for Wireless Link
*
* \author              Atmel Corporation: http://www.atmel.com \n
*                      Support email: avr@atmel.com
*
* $Name:  $
* $Revision: 1686 $
* $Date: 2007-04-30 12:01:30 +0200 (ma, 30 apr 2007) $
*
* Copyright (c) 2006, Atmel Corporation All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* 3. The name of ATMEL may not be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY ATMEL ``AS IS'' AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE EXPRESSLY AND
* SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT,
* INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
#include "comms.h"
#include "common.h"
#include "config.h"



//! Pointer to next byte to be transmitted.
static const byte * volatile nextUSIByte;
//! Number of bytes left to transmit.
static volatile byte USIBytesLeft;



#ifdef USE_UART

//! Part two of UART byte frame.
static byte uartPartTwo;

//! Put next byte into USI Data Register or stop transmitting.
#pragma vector = USI_OVF_vect
__interrupt void USIOverflowHandler(void)
{
	if( uartPartTwo != 0x00 ) { // Part two needs to be transmitted?
		USIDR = (USIDR&0xe0) | uartPartTwo; // OR it into USI Data register.
		USISR = (1<<USIOIF) | (16-6); // Seed timer to overflow in 6 bit shifts.
		uartPartTwo = 0x00; // Clear part two, indicating that it has been sent.
	} else {
		--USIBytesLeft; // One less byte left to transmit.

		if( USIBytesLeft > 0 ) {
			byte tempByte = *nextUSIByte++;
			// Put next byte in output register.
			USIDR = 0x80 |
				((tempByte<<5)&(1<<5)) |
				((tempByte<<3)&(1<<4)) |
				((tempByte<<1)&(1<<3)) |
				((tempByte>>1)&(1<<2)) |
				((tempByte>>3)&(1<<1)) |
				((tempByte>>5)&(1<<0));
			// Prepare part two for next interrupt.
			uartPartTwo = 0x07 |
				((tempByte>>2)&(1<<4)) |
				((tempByte>>4)&(1<<3));

			// Clear overflow flag and seed USI timer
			// to overflow in 5 bit shifts.
			USISR = (1<<USIOIF) | (16-5);

		} else {
			// Stop timer providing clock to USI module.
			TCCR0B = 0x00;
			// Clear USI overflow flag.
			USISR = (1<<USIOIF);
		}
	}
}



void startTransmission( const byte * buffer, byte byteCount )
{
	// Prepare control variables.
	byte tempByte = *buffer++;
	nextUSIByte = buffer;
	USIBytesLeft = byteCount;

	USIDR = 0x80 |
		((tempByte<<5)&(1<<5)) |
		((tempByte<<3)&(1<<4)) |
		((tempByte<<1)&(1<<3)) |
		((tempByte>>1)&(1<<2)) |
		((tempByte>>3)&(1<<1)) |
		((tempByte>>5)&(1<<0));
	uartPartTwo = 0x07 |
		((tempByte>>2)&(1<<4)) |
		((tempByte>>4)&(1<<3));

	// Clear overflow flag and seed USI timer
	// to overflow in 5 bit shifts.
	USISR = (1<<USIOIF) | (16-5);

	// Start USI in Three-wire mode clocked by
	// Timer/Counter0 Compare Match A. Overflow interrupt enabled.
	USICR = (1<<USIOIE) |
		(0<<USIWM1) | (1<<USIWM0) |   // Three-wire mode, data output on PB1.
		(0<<USICS1) | (1<<USICS0);

	// Reset Timer/Counter0 value.
	TCNT0 = 0x00;
	// Set data rate to 19200 bps (ie. 9600 bps with Manchester).
	OCR0A = 51; // approx. (8MHz/8/19200bps)-1, ref. CTC Mode in datasheet.
	TCCR0A = (1<<WGM01);
	// Start Timer/Counter0 with CPU clock / 8.
	TCCR0B = (0<<CS02) | (1<<CS01) | (0<<CS00);
}


void continueTransmission( const byte * buffer, byte byteCount )
{
	startTransmission( buffer, byteCount );
}


#else



//! Part two of Manchester encoded byte.
static byte manchesterPartTwo;
//! Part three of Manchester encoded byte.
static byte manchesterPartThree;

//! Lookup table for converting 4 bits to Manchester encoding.
static const byte __flash manchesterNibble[ 16 ] = {
#ifndef INVERTED_MANCHESTER
	0x55, // 01 01 01 01
	0x56, // 01 01 01 10
	0x59, // 01 01 10 01
	0x5a, // 01 01 10 10

	0x65, // 01 10 01 01
	0x66, // 01 10 01 10
	0x69, // 01 10 10 01
	0x6a, // 01 10 10 10

	0x95, // 10 01 01 01
	0x96, // 10 01 01 10
	0x99, // 10 01 10 01
	0x9a, // 10 01 10 10

	0xa5, // 10 10 01 01
	0xa6, // 10 10 01 10
	0xa9, // 10 10 10 01
	0xaa  // 10 10 10 10
#else
	0xaa, // 10 10 10 10
	0xa9, // 10 10 10 01
	0xa6, // 10 10 01 10
	0xa5, // 10 10 01 01

	0x9a, // 10 01 10 10
	0x99, // 10 01 10 01
	0x96, // 10 01 01 10
	0x95, // 10 01 01 01

	0x6a, // 01 10 10 10
	0x69, // 01 10 10 01
	0x66, // 01 10 01 10
	0x65, // 01 10 01 01

	0x5a, // 01 01 10 10
	0x59, // 01 01 10 01
	0x56, // 01 01 01 10
	0x55  // 01 01 01 01
#endif
};



//! Put next byte into USI Data Register or stop transmitting.
#pragma vector = USI_OVF_vect
__interrupt void USIOverflowHandler(void)
{
	// 'manchesterPartTwo' is non-zero if part two has not been sent.
	if( manchesterPartTwo != 0x00 ) {
		USISR = (1<<USIOIF) | (16-7);
		USIDR = manchesterPartTwo;
		manchesterPartTwo = 0x00;

		// 'manchesterPartThree' is non-zero if part three has not been sent.
	} else if( manchesterPartThree != 0x00 ) {
		USISR = (1<<USIOIF) | (16-2);
		USIDR = manchesterPartThree;
		manchesterPartThree = 0x00;

		// If part two and three are zero, it's time to prepare next byte.
	} else {
		// Clear overflow flag and seed USI counter.
		// to overflow in 7 bit shifts. It is important to
		// seed the timer this early, so that the computations
		// below does not affect the transmission timing.
		USISR = (1<<USIOIF) | (16-7);

		--USIBytesLeft; // One less byte left to transmit.

		// If bytes left, prepare for sending the next.
		if( USIBytesLeft > 0 ) {
			byte currentByte = *nextUSIByte++;
			byte manchesterByte1;
			byte manchesterByte2;

			// Prepare Manchester pattern.
			manchesterByte1 = manchesterNibble[ currentByte >> 4 ];
			manchesterByte2 = manchesterNibble[ currentByte & 0x0f ];

			// Put bit 0-6 of pattern at the end of USI data register.
			USIDR = USIDR & 0x80 | (manchesterByte1 >> 1);

			// Prepare part two of pattern, ie. bits 6-13.
			manchesterPartTwo = (manchesterByte1 << 6) | (manchesterByte2 >> 2);

			// Prepare part three of pattern, ie. bits 13-15 and 5 dummy zero bits.
			manchesterPartThree = (manchesterByte2 << 5);
		} else {
			// No bytes left => turn off further interrupts, in case
			// the call to stopTransmission is delayed.
			// The USI module will continue transmittion zeros.
			// We cannot stop the whole USI module, since the user
			// might want to continue transmitting. In that case, the
			// user must call continueTransmission as soon as possible
			// after returning from waitForTransmission.
			USICR &= ~(1<<USIOIE); // Disable interrupts.
		}
	}
}



void startTransmission( const byte * buffer, byte byteCount )
{
	// Prepare control variables.
	byte currentByte = *buffer++;
	nextUSIByte = buffer;
	USIBytesLeft = byteCount;

	// Prepare Manchester pattern.
	byte manchesterByte1 = manchesterNibble[ currentByte >> 4 ];
	byte manchesterByte2 = manchesterNibble[ currentByte & 0x0f ];

	// Put idle bit value and bit 0-6 of pattern in USI data register.
	// This is the first part of the pattern. The output pin changes immediately.
	USIDR = 0x80 | (manchesterByte1 >> 1);

	// Prepare part two of pattern, ie. bits 6-13.
	manchesterPartTwo = (manchesterByte1 << 6) | (manchesterByte2 >> 2);

	// Prepare part three of pattern, ie. bits 13-15 and 5 dummy zero bits.
	manchesterPartThree = (manchesterByte2 << 5);

	// Clear overflow flag and seed USI timer
	// to overflow in 7 bit shifts, so that last bit of first part of pattern is left.
	USISR = (1<<USIOIF) | (16-7);

	// Start USI in Three-wire mode clocked by
	// Timer/Counter0 Compare Match A. Overflow interrupt enabled.
  #ifdef ASK
	USICR = (1<<USIOIE) |
		(0<<USIWM1) | (1<<USIWM0) |   // Three-wire mode, data output on PB1.
		(0<<USICS1) | (1<<USICS0);
  #endif
  #ifdef FSK
	USICR = (1<<USIOIE) |
		(1<<USIWM1) | (0<<USIWM0) |   // Two-wire mode, data output on PB0 (open collector).
		(0<<USICS1) | (1<<USICS0);
	DDRB |= (1<<PB0); // Enable data line output open collector driver.
	PORTB |= ((1<<PB1) | (1<<PB0)); // Turn on carrier wave and give data line to USI module.
  #endif

  #ifdef BPS_19200
	// Set data rate to 19200 bps (ie. 9600 bps with Manchester).
	TCNT0 = 0x00;
	OCR0A = 51; // approx. (8MHz/8/19200bps)-1, ref. CTC Mode in datasheet.
	TCCR0A = (1<<WGM01); // Clear on Compare Match.
	TCCR0B = (0<<CS02) | (1<<CS01) | (0<<CS00); // Prescaler factor 8.
  #endif
  #ifdef BPS_9600
	// Set data rate to 9600 bps (ie. 4800 bps with Manchester).
	TCNT0 = 0x00;
	OCR0A = 103; // approx. (8MHz/8/9600bps)-1, ref. CTC Mode in datasheet.
	TCCR0A = (1<<WGM01); // Clear on Compare Match.
	TCCR0B = (0<<CS02) | (1<<CS01) | (0<<CS00); // Prescaler factor 8.
  #endif
  #ifdef BPS_4800
	// Set data rate to 4800 bps (ie. 2400 bps with Manchester).
	TCNT0 = 0x00;
	OCR0A = 25; // approx. (8MHz/64/2400bps)-1, ref. CTC Mode in datasheet.
	TCCR0A = (1<<WGM01); // Clear on Compare Match.
	TCCR0B = (0<<CS02) | (1<<CS01) | (1<<CS00); // Prescaler factor 64.
  #endif
  #ifdef BPS_2400
	// Set data rate to 2400 bps (ie. 1200 bps with Manchester).
	TCNT0 = 0x00;
	OCR0A = 51; // approx. (8MHz/64/2400bps)-1, ref. CTC Mode in datasheet.
	TCCR0A = (1<<WGM01); // Clear on Compare Match.
	TCCR0B = (0<<CS02) | (1<<CS01) | (1<<CS00); // Prescaler factor 64.
  #endif
}



void continueTransmission( const byte * buffer, byte byteCount )
{
	// Prepare control variables.
	byte currentByte = *buffer++;
	nextUSIByte = buffer;
	USIBytesLeft = byteCount;

	// Prepare Manchester pattern.
	byte manchesterByte1 = manchesterNibble[ currentByte >> 4 ];
	byte manchesterByte2 = manchesterNibble[ currentByte & 0x0f ];

	// Put bit 0-6 of pattern at the end of USI data register.
	USIDR = USIDR & 0x80 | (manchesterByte1 >> 1);

	// Prepare part two of pattern, ie. bits 6-13.
	manchesterPartTwo = (manchesterByte1 << 6) | (manchesterByte2 >> 2);

	// Prepare part three of pattern, ie. bits 13-15 and 5 dummy zero bits.
	manchesterPartThree = (manchesterByte2 << 5);

	// Enable USI interrupts again.
	USICR |= (1<<USIOIE);
}



#endif // #ifdef USE_UART



void stopTransmission()
{
	USICR = 0x00;
  #ifdef FSK
	PORTB &= ~((1<<PB1) | (1<<PB0)); // Turn off carrier wave and release data line.
	DDRB &= ~(1<<PB0); // Disable data line output open collector driver.
  #endif
}



void waitForTransmission()
{
	// Enter Idle sleep mode while waiting for transmission to finish.
	MCUCR = (1<<SE) | (0<<SM1) | (0<<SM0);
	while( USIBytesLeft ) { __sleep(); }
}

