// This file has been prepared for Doxygen automatic documentation generation.
/*! \file ********************************************************************
*
* \brief  Source file for application common functions.
*
* This file contains the function implementation for common
* functions needed by the Remote Keyless Entry application.
* Refer to the common.h file for more details.
*
* - File:              common.c
* - Compiler:          IAR EWAVR 4.11B
* - Supported devices: ATtiny45/85
* - AppNote:           AVR411 - Secure Rolling Code Algorithm
*                      for Wireless Link
*
* \author              Atmel Corporation: http://www.atmel.com \n
*                      Support email: avr@atmel.com
*
* $Name:  $
* $Revision: 1193 $
* $Date: 2006-10-31 14:21:08 +0100 (ti, 31 okt 2006) $
*
* Copyright (c) 2006, Atmel Corporation All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* 3. The name of ATMEL may not be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY ATMEL ``AS IS'' AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE EXPRESSLY AND
* SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT,
* INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
#include "common.h"




void copyBytes( byte * destination, const byte * source, byte count)
{
  // Copy to temporary variables for optimization.
  FLASH_Unlock(FLASH_MEMTYPE_DATA);
  FLASH_Unlock(FLASH_MEMTYPE_PROG);

  byte * tempDest = destination;
  const byte * tempSrc = source;
  byte tempCount = count;
  
  do {
    *tempDest++ = *tempSrc++;
  } while( --tempCount );
   FLASH_Lock(FLASH_MEMTYPE_DATA);
FLASH_Lock(FLASH_MEMTYPE_PROG);
}



void addConstantFromEEPROM( byte * bytes,
                           const byte __eeprom * constant, byte count )
{
  // Copy to temporary variables for optimization.
  byte * tempDestination = bytes;
  byte tempCount = count;
  byte tempValue;
  //EEAR = (unsigned short int) constant;
  
  do {
//    EECR |= (1<<EERE);
//    ++EEAR;
//    tempValue = *tempDestination ^ EEDR; // Add in GF(2), ie. XOR.
    tempValue=*tempDestination ^ FLASH_ReadByte((uint32_t)constant);
    *tempDestination++ = tempValue;
  } while( --tempCount );
}



void copyBytesToEEPROM( byte __eeprom * destination,
                       const byte * source, byte count )
{
  
  // Copy to temporary variables for optimization.
  const byte * tempSrc = source;
  byte tempCount = count;
  FLASH_Unlock(FLASH_MEMTYPE_DATA);
  do {
    
    FLASH_ProgramByte((uint32_t)destination,*tempSrc);
    tempSrc++;
    //		EEDR = *tempSrc++;
    //		EECR |= (1<<EEMWE);
    //		EECR |= (1<<EEWE);
    //		do {} while( EECR & (1<<EEWE) );
    //		++EEAR;
  } while( --tempCount );
  FLASH_Lock(FLASH_MEMTYPE_DATA);
}



void copyBytesFromEEPROM( byte * destination,
                         const byte __eeprom * source, byte count )
{
  // Copy to temporary variables for optimization.
  byte * tempDest = destination;
  byte tempCount = count;
  
 //EEAR = (unsigned short int) source;
  
  do {
     *tempDest++ =FLASH_ReadByte((uint32_t)source);
//    EECR |= (1<<EERE);
//    ++EEAR;
//    *tempDest++ = EEDR;
  } while( --tempCount );
}



