// This file has been prepared for Doxygen automatic documentation generation.
/*! \file ********************************************************************
*
* \brief  Header file for communications public functions.
*
* This file contains the prototypes for the communications control functions.
* The implementation is found in the comms.c file.
* The communication is interrupt-controlled and is initiated with the
* startTransmission() function. To add more data to the transmission before
* it has finished, use the continueTransmission() function. To wait for an
* ongoing transmission to finish, use the waitForTransmisssion() function.
* Always call the stopTransmission() function when you do not want to transmit
* more data. This turns off the RF transmitter.
*
* - File:              comms.h
* - Compiler:          IAR EWAVR 4.11B
* - Supported devices: ATtiny45/85
* - AppNote:           AVR411 - Secure Rolling Code Algorithm
*                      for Wireless Link
*
* \author              Atmel Corporation: http://www.atmel.com \n
*                      Support email: avr@atmel.com
*
* $Name:  $
* $Revision: 1193 $
* $Date: 2006-10-31 14:21:08 +0100 (ti, 31 okt 2006) $
*
* Copyright (c) 2006, Atmel Corporation All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* 3. The name of ATMEL may not be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY ATMEL ``AS IS'' AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE EXPRESSLY AND
* SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT,
* INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
#ifndef COMMS_H
#define COMMS_H

#include "common.h"
#include "config.h"



//! Start transmitting 'byteCount' bytes from 'buffer' in SRAM using Manchester encoding.
void startTransmission( const byte * buffer, byte byteCount );
//! Add 'byteCount' bytes from 'buffer' in SRAM to an already running transmission.
void continueTransmission( const byte * buffer, byte byteCount );
//! Sleep and wait for a running transmission to finish.
void waitForTransmission();
//! Stop transmission module and restore previous port states.
void stopTransmission();



#endif

