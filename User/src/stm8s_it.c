/**
******************************************************************************
* @file     stm8s_it.c
* @author   MCD Application Team
* @version  V2.2.0
* @date     30-September-2014
* @brief    Main Interrupt Service Routines.
******************************************************************************
* @attention
*
* <h2><center>&copy; COPYRIGHT 2014 STMicroelectronics</center></h2>
*
* Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
* You may not use this file except in compliance with the License.
* You may obtain a copy of the License at:
*
*        http://www.st.com/software_license_agreement_liberty_v2
*
* Unless required by applicable law or agreed to in writing, software 
* distributed under the License is distributed on an "AS IS" BASIS, 
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
******************************************************************************
*/ 

/* Includes ------------------------------------------------------------------*/
#include "stm8s_it.h"
#include "GenericQueue.h"
#include "math.h"
extern GenericQueue_Type queue;
extern uint8_t iapsr;
extern bool rstaccess;

//extern uint8_t reply_packet[UART_BUFF_SIZE]; 

/** @addtogroup UART1_HyperTerminal_Interrupt
* @{
*/
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define TX_BUFFER_SIZE (countof(TxBuffer) - 1)

/* Private macro -------------------------------------------------------------*/
#define countof(a)   (sizeof(a) / sizeof(*(a)))
/* Private variables ---------------------------------------------------------*/
uint8_t TxBuffer[] = "HyperTerminal Interrupt: UART1-Hyperterminal communication using Interrupt";
__IO uint8_t TxCounter = 0;
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/* Public functions ----------------------------------------------------------*/


#ifdef _COSMIC_
/**
* @brief  Dummy interrupt routine
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(NonHandledInterrupt, 25)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
}
#endif /*_COSMIC_*/

/**
* @brief  TRAP interrupt routine
* @param  None
* @retval None
*/
INTERRUPT_HANDLER_TRAP(TRAP_IRQHandler)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
}
/**
* @brief  Top Level Interrupt routine
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(TLI_IRQHandler, 0)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
}

/**
* @brief  Auto Wake Up Interrupt routine
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(AWU_IRQHandler, 1)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
}

/**
* @brief  Clock Controller Interrupt routine
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(CLK_IRQHandler, 2)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
}

/**
* @brief  External Interrupt PORTA Interrupt routine
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(EXTI_PORTA_IRQHandler, 3)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
}

/**
* @brief  External Interrupt PORTB Interrupt routine
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(EXTI_PORTB_IRQHandler, 4)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
}

/**
* @brief  External Interrupt PORTC Interrupt routine
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(EXTI_PORTC_IRQHandler, 5)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
}

/**
* @brief  External Interrupt PORTD Interrupt routine
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(EXTI_PORTD_IRQHandler, 6)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
}

/**
* @brief  External Interrupt PORTE Interrupt routine
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(EXTI_PORTE_IRQHandler, 7)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
}
#ifdef STM8S903
/**
* @brief  External Interrupt PORTF Interrupt routine
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(EXTI_PORTF_IRQHandler, 8)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
}
#endif /*STM8S903*/

#if defined (STM8S208) || defined (STM8AF52Ax)
/**
* @brief CAN RX Interrupt routine.
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(CAN_RX_IRQHandler, 8)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
}

/**
* @brief  CAN TX Interrupt routine
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(CAN_TX_IRQHandler, 9)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
}
#endif /*STM8S208 || STM8AF52Ax */

/**
* @brief  SPI Interrupt routine
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(SPI_IRQHandler, 10)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
}

/**
* @brief  Timer1 Update/Overflow/Trigger/Break Interrupt routine
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(TIM1_UPD_OVF_TRG_BRK_IRQHandler, 11)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
  static uint8_t i;
  
  unsigned char byte=0;
  unsigned short dutyval=0;
  
  
  if(!GQueue_EmptyCheck(&queue))
    {
       
      GQueue_FrontRead(&queue,&byte); 
      if((uint8_t)pow(2,7-i)& byte)
      {
        // UART1_SendData8('(');
        dutyval=750;
      }
      else
      {
        //UART1_SendData8(')');
        dutyval=250;
      }
      
      TIM1_SetCompare4(dutyval);        
      TIM1_CtrlPWMOutputs(ENABLE);

     //UART1_SendData8(dutyval);
      // TIM1_Cmd(ENABLE);
      i++;
    if(i==8)
    {
      i=0;
      GQueue_Remove(&queue,&byte);
    }
    }
  else
  {
    TIM1_CtrlPWMOutputs(DISABLE);
    //GQueue_Add(&queue,0xf0);
    //TIM1_Cmd(DISABLE);
    //////////// 
    GPIO_WriteLow(GPIOC,GPIO_PIN_4);
  } 
  
  TIM1_ClearFlag(TIM1_FLAG_UPDATE);
  
}

/**
* @brief  Timer1 Capture/Compare Interrupt routine
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(TIM1_CAP_COM_IRQHandler, 12)
{
  
  // UART1_SendData8('c');
  //GPIO_WriteReverse(GPIOD,GPIO_PIN_3);
  //  static int i;
  //  unsigned char byte=0;
  //  unsigned short dutyval=0;
  //  
  //    if(i==8&&!GQueue_EmptyCheck(&queue))
  //    { 
  //      i=0;
  //    GQueue_Remove(&queue,&byte);
  //    }
  //  if(!GQueue_EmptyCheck(&queue))
  //  {
  //   
  //    GQueue_FrontRead(&queue,&byte); 
  //    if((uint8_t)pow(2,7-i)& byte)
  //    {
  //    //  UART1_SendData8('(');
  //      dutyval=750;
  //    }
  //    else
  //    {
  //      //UART1_SendData8(')');
  //      dutyval=250;
  //    }
  //    TIM1_SetCompare4(dutyval);  
  //    TIM1_CtrlPWMOutputs(ENABLE);
  //   // TIM1_Cmd(ENABLE);
  //    i++;
  // 
  //  }
  //  else
  //  {
  //       //TIM1_Cmd(DISABLE);
  //    TIM1_CtrlPWMOutputs(DISABLE);
  //    //GPIO_WriteLow(GPIOC,GPIO_PIN_4);
  //  } 
  //  
  // TIM1_ClearFlag(TIM1_FLAG_CC4);
  
}

#ifdef STM8S903
/**
* @brief  Timer5 Update/Overflow/Break/Trigger Interrupt routine
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(TIM5_UPD_OVF_BRK_TRG_IRQHandler, 13)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
  
  
}
/**
* @brief  Timer5 Capture/Compare Interrupt routine
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(TIM5_CAP_COM_IRQHandler, 14)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
}

#else /*STM8S208, STM8S207, STM8S105 or STM8S103 or STM8AF62Ax or STM8AF52Ax or STM8AF626x */
/**
* @brief  Timer2 Update/Overflow/Break Interrupt routine
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(TIM2_UPD_OVF_BRK_IRQHandler, 13)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
  if(rstaccess)
  {
    UART1_SendData8('R');
    Delay(50);
    WWDG_SWReset();
  }
  //rstaccess=TRUE;
  TIM2_ClearFlag(TIM2_FLAG_UPDATE);
}
/**
* @brief  Timer2 Capture/Compare Interrupt routine
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(TIM2_CAP_COM_IRQHandler, 14)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
  static uint8_t val[8];
  static uint16_t  start,width,period;
  uint16_t pwm,temp;  
  float tempf;
  static uint8_t i=0;
  
  
  if( TIM2->CCER1 & TIM2_CCER1_CC1P )// CHANGE TO RISING EDGE 
  { //TIM2->CCER1 &= ~TIM2_CCER1_CC1P;
    TIM2_OC1PolarityConfig(TIM2_OCPOLARITY_HIGH);
    if(TIM2_GetCapture1()>start)
    width=TIM2_GetCapture1()-start;
    else
      width=start-TIM2_GetCapture1();
    //  UART1_SendData8('#');
   
    //UART1_SendData8((uint8_t)width);
  }
  else // CAHANGE TO FALLING EDGE 
  {
    //TIM2->CCER1 |= TIM2_CCER1_CC1P;
    //UART1_SendData8('*');
    //if(TIM2_GetCapture1()>58000)
    //TIM2_OC1PolarityConfig(TIM2_OCPOLARITY_LOW);
    TIM2->CCER1 |= (uint8_t)TIM2_CCER1_CC1P;
    temp=TIM2_GetCapture1();
    // period=abs(temp-start);
    if(temp>start)
    period=temp-start;
    else
      period=start-temp;
    tempf=width/(float)period;
    pwm=(uint16_t)(tempf*100);
    start=temp;
    val[i]=pwm;
    UART1_SendData8((uint8_t)pwm);
    //       if(start>40000)
    //       { 
    //         start=0;
    //         TIM2_SetCounter(TIM2_GetCounter()-TIM2_GetCapture1());
    //       }        
    //       
    //      if(pwm>15&&pwm<35)
    //        UART1_SendData8('O');
    //      else if(pwm>55&&pwm<75)
    //        UART1_SendData8('I');
    //      if(!secondrise)
    //        secondrise=true;
    //      else 
    //        secondrise=false;
    i++;
     if(i==8)
     {
       //usend(8,&val);
      i=0;
     }
  }
  
  
  TIM2_ClearFlag(TIM2_FLAG_CC1);
  
}
#endif /*STM8S903*/

#if defined (STM8S208) || defined(STM8S207) || defined(STM8S007) || defined(STM8S105) || \
defined(STM8S005) ||  defined (STM8AF62Ax) || defined (STM8AF52Ax) || defined (STM8AF626x)
/**
* @brief Timer3 Update/Overflow/Break Interrupt routine.
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(TIM3_UPD_OVF_BRK_IRQHandler, 15)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
}

/**
* @brief  Timer3 Capture/Compare Interrupt routine
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(TIM3_CAP_COM_IRQHandler, 16)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
}
#endif /*STM8S208, STM8S207 or STM8S105 or STM8AF62Ax or STM8AF52Ax or STM8AF626x */

#if defined (STM8S208) || defined(STM8S207) || defined(STM8S007) || defined(STM8S103) || \
defined(STM8S003) ||  defined (STM8AF62Ax) || defined (STM8AF52Ax) || defined (STM8S903)
/**
* @brief  UART1 TX Interrupt routine
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(UART1_TX_IRQHandler, 17)
{
  /* Write one byte to the transmit data register */
  //UART1_SendData8(TxBuffer[TxCounter++]);
  
  //if (TxCounter == TX_BUFFER_SIZE)
  //{
  /* Disable the USART Transmit Complete interrupt */
  // UART1_ITConfig(UART1_IT_TXE, DISABLE);
  //}
}

/**
* @brief  UART1 RX Interrupt routine
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(UART1_RX_IRQHandler, 18)
{
  /* Read one byte from the receive data register */
  //    reply_packet[RxCounter++] = UART1_ReceiveData8();
  //
  //    if (RxCounter == UART_BUFF_SIZE)
  //    {
  //        /* Disable the UART1 Receive interrupt */
  //        UART1_ITConfig(UART1_IT_RXNE, DISABLE);
  //        RxCounter=0;
  // }
  
  
}
#endif /*STM8S105*/

/**
* @brief  I2C Interrupt routine
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(I2C_IRQHandler, 19)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
}

#if defined(STM8S105) || defined(STM8S005) ||  defined (STM8AF626x)
/**
* @brief  UART2 TX interrupt routine.
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(UART2_TX_IRQHandler, 20)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
}

/**
* @brief  UART2 RX interrupt routine.
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(UART2_RX_IRQHandler, 21)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
}
#endif /* STM8S105*/

#if defined(STM8S207) || defined(STM8S007) || defined(STM8S208) || defined (STM8AF52Ax) || defined (STM8AF62Ax)
/**
* @brief  UART3 TX interrupt routine.
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(UART3_TX_IRQHandler, 20)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
}

/**
* @brief  UART3 RX interrupt routine.
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(UART3_RX_IRQHandler, 21)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
}
#endif /*STM8S208 or STM8S207 or STM8AF52Ax or STM8AF62Ax */

#if defined(STM8S207) || defined(STM8S007) || defined(STM8S208) || defined (STM8AF52Ax) || defined (STM8AF62Ax)
/**
* @brief  ADC2 interrupt routine.
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(ADC2_IRQHandler, 22)
{
  
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
  return;
  
}
#else /*STM8S105, STM8S103 or STM8S903 or STM8AF626x */
/**
* @brief  ADC1 interrupt routine.
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(ADC1_IRQHandler, 22)
{
  
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
  return;
  
}
#endif /*STM8S208 or STM8S207 or STM8AF52Ax or STM8AF62Ax */

#ifdef STM8S903
/**
* @brief  Timer6 Update/Overflow/Trigger Interrupt routine
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(TIM6_UPD_OVF_TRG_IRQHandler, 23)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
}
#else /*STM8S208, STM8S207, STM8S105 or STM8S103 or STM8AF62Ax or STM8AF52Ax or STM8AF626x */
/**
* @brief  Timer4 Update/Overflow Interrupt routine
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(TIM4_UPD_OVF_IRQHandler, 23)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
}
#endif /*STM8S903*/

/**
* @brief  Eeprom EEC Interrupt routine
* @param  None
* @retval None
*/
INTERRUPT_HANDLER(EEPROM_EEC_IRQHandler, 24)
{
  /* In order to detect unexpected events during development,
  it is recommended to set a breakpoint on the following instruction.
  */
  iapsr=FLASH->IAPSR;
  //UART1_SendData8((uint8_t)(tik/0x0f));
  //  if(iapsr&0x04)
  //     UART1_SendData8('E');
  //  else if(iapsr&0x01)
  //     UART1_SendData8('w');  
  
  FLASH_ITConfig(DISABLE);
  
  
}

/**
* @}
*/


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
