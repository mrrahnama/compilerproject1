// This file has been prepared for Doxygen automatic documentation generation.
/*! \file ********************************************************************
*
* \brief  Source file for CRC public and support functions.
*
* This file contains the function implementation for the CRC
* generation functions. Refer to the crc.h file for more details.
*
* - File:              crc.c
* - Compiler:          IAR EWAVR 4.11B
* - Supported devices: ATtiny45/85
* - AppNote:           AVR411 - Secure Rolling Code Algorithm
*                      for Wireless Link
*
* \author              Atmel Corporation: http://www.atmel.com \n
*                      Support email: avr@atmel.com
*
* $Name:  $
* $Revision: 1193 $
* $Date: 2006-10-31 14:21:08 +0100 (ti, 31 okt 2006) $
*
* Copyright (c) 2006, Atmel Corporation All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* 3. The name of ATMEL may not be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY ATMEL ``AS IS'' AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE EXPRESSLY AND
* SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT,
* INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
#include "crc.h"
#include "common.h"
#include "config.h"



byte CRCReg[3];



void initCRC(void)
{
	CRCReg[0] = 0xff;
	CRCReg[1] = 0xff;
	CRCReg[2] = 0x00; // Always 0x00. See comment in crc.h file.
}



void calcCRC( byte nextByte )
{
	byte reg0 = CRCReg[0];
	byte reg1 = CRCReg[1];
	byte tempByte = nextByte;
	byte count = 8;
	do {
		if( reg0 >> 7 ) {
			reg0 = (reg0 << 1) | (reg1 >> 7);
			reg1 = (reg1 << 1) | (tempByte >> 7);
			tempByte <<= 1;

			reg0 ^= CRC_POLY >> 8;
			reg1 ^= CRC_POLY & 0xff;
		} else {
			reg0 = (reg0 << 1) | (reg1 >> 7);
			reg1 = (reg1 << 1) | (tempByte >> 7);
			tempByte <<= 1;
		}
	} while( --count );

	CRCReg[0] = reg0;
	CRCReg[1] = reg1;
}

