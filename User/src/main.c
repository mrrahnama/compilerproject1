
/* Includes ------------------------------------------------------------------*/
#include "stm8s.h"
#include "stdio.h"
#include "math.h"
#include "GenericQueue.h"
#include "aes.h"
#include "cmac.h"

#define LED_ERROR PD_ODR2
#define MAX_WDGRST_COUNTER  4 
#define CCR4_Val  ((u16)300) // Configure channel 4 Pulse Width For TIM1
#define BUTTON_PINS 0x18
#define Teach_Command 0x18

GenericQueue_Type queue;
unsigned char qbuffer[16];

__no_init __eeprom uint32_t seqcounter;
__no_init __eeprom uint8_t  secretkey[16];
__no_init __eeprom uint32_t serialno;
__no_init __eeprom uint8_t wwdgtsrcounter;
__no_init __eeprom uint8_t sharedkey[16];

uint8_t cryptoblock[16];

uint8_t iapsr;
bool rstaccess;


struct MessagePayload
{
  byte preamble;
  SERIAL_NO_TYPE serialNo;
  SEQ_COUNTER_TYPE counterValue;
  COMMAND_CODE_TYPE commandCode;
};

union SharedBlock {
  struct MessagePayload messagePayload;
  byte cryptoBlock[ BLOCK_SIZE ];
}sharedblock;
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
static void Clock_Config(void);
static void Config(void);
void Delay(uint32_t nCount);
void update_eeprom(uint32_t varaddress,uint32_t ramval);
byte getcommand(void);
void creatmessage(void);
void usend(byte count,byte * data);
void resetcheck(void);
/**
* @brief firmware main entry point.
* @par Parameters:
* None
* @retval 
* None
*/
void main(void)
{
  //resetcheck();
  rstaccess=FALSE;
  char ch=0xaa;
  byte command=0;
  GQueue_Init(&queue,16,qbuffer);
  GQueue_Add(&queue,ch);
  /*Config Core Clock At HSE(Ext Osc) 16Mhz*/
  Clock_Config();
  /* Config 3PWMs With Freq of 2Khz For: TIM1_CH3=>> PC3, TIM1_CH4 = >>PC4, TIM2_CH2 = >>PD3*/
  Config();

//  update_eeprom((uint32_t)&seqcounter,0);
//  update_eeprom((uint32_t)&serialno,0x20202020);
//  for(byte i=0;i<16;i++)
//  {
//      FLASH_Unlock(FLASH_MEMTYPE_DATA);
//      FLASH_ProgramByte((uint32_t)secretkey+i,i+1); 
//      FLASH_Lock(FLASH_MEMTYPE_DATA);
//
//  }
  //update_eeprom((uint32_t)&secretkey,0xfefefefe);
  
  UART1_SendData8('g');
  //char test=(uint8_t) seqcounter;
  //creatmessage();
 //char str[]="salam";
  //usend(5,(byte *)str);
              GQueue_Add(&queue,0x00);
                 GQueue_Add(&queue,0x01);
                   

  while(1)
  {
    
      Delay(1000000);
      GPIO_WriteReverse(GPIOD, GPIO_PIN_3);
//      Delay(60000);
//      GPIO_WriteReverse(GPIOD, GPIO_PIN_2);
      GQueue_Add(&queue,0x00);//peamble byte
      GQueue_Add(&queue,0x30);
      GQueue_Add(&queue,0x40);
      GQueue_Add(&queue,0x50);
      GQueue_Add(&queue,0x60);      
      GQueue_Add(&queue,0x70);
      GQueue_Add(&queue,0x80);
      GQueue_Add(&queue,0x90);
      GQueue_Add(&queue,0xa0);
      GQueue_Add(&queue,0xb0);
      GQueue_Add(&queue,0xc0);
      GQueue_Add(&queue,0xd0);
      GQueue_Add(&queue,0xe0);
      GQueue_Add(&queue,0xf0);
      
//      GQueue_Add(&queue,0x29);
//      GQueue_Add(&queue,0x3a);
//      GQueue_Add(&queue,0x4b);


    //UART1_SendData8('g');
    //high level source code
    /*
    TIM2_SetCounter(0);
    UART1_SendData8('F');
    // usend(5,(byte *)str);
    // wfi();
    command=getcommand();
    sharedblock.messagePayload.commandCode=command;
    calcCMAC((const byte *)&sharedblock.messagePayload.serialNo,cryptoblock);
    usend(9,(byte *)&sharedblock.messagePayload.serialNo);
    usend(4,cryptoblock+12);
    update_eeprom((uint32_t)&seqcounter,seqcounter+1);
    sharedblock.messagePayload.counterValue=seqcounter;
*/
    //high level code source
    //   UART1_SendData8('-');
    //   Delay(5000);
    //   wfi();
    //  updateseqcounter((uint32_t)test++);
    //  waitforsequpdate();    
  }
}

void resetcheck()
{
  if(RST->SR&RST_SR_WWDGF) //watchdog reset
  {
    if(wwdgtsrcounter < MAX_WDGRST_COUNTER)
    {
      FLASH_Unlock(FLASH_MEMTYPE_DATA);
      FLASH_ProgramByte((uint32_t)&wwdgtsrcounter, wwdgtsrcounter+1);
      FLASH_Lock(FLASH_MEMTYPE_DATA);
    }
    else
    {
      for(u8 i=0;i<20;i++)
      {
        GPIO_WriteReverse(GPIOD, GPIO_PIN_2);
        Delay(50000);
      }  
      halt();// go to halt mode to power reset with user and save battery 
    }
  }
  else
  {
    FLASH_Unlock(FLASH_MEMTYPE_DATA);
    FLASH_ProgramByte((uint32_t)&wwdgtsrcounter, 0);
    FLASH_Lock(FLASH_MEMTYPE_DATA);
  } 
}
void update_eeprom(uint32_t varaddress,uint32_t ramval)
{
  FLASH_Unlock(FLASH_MEMTYPE_DATA);
  FLASH_ProgramWord(varaddress,ramval); 
  FLASH_Lock(FLASH_MEMTYPE_DATA);
}



/**
* @brief Config System Clock.
* @par Parameters:
* None
* @retval 
* None
*/

static void Clock_Config(void)
{
  //CLK_DeInit();
  
  /* Clock divider to HSI/1 */
  CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);
  
  /*Enable External Crystal Oscillator*/
  //CLK_ClockSwitchConfig(CLK_SWITCHMODE_AUTO, CLK_SOURCE_HSE, DISABLE, CLK_CURRENTCLOCKSTATE_DISABLE);  
}

/**
* @brief Config Timers.
* @par Parameters:
* None
* @retval 
* None
*/
static void Config(void)
{
  
  //GPIO_DeInit(GPIOD);
  GPIO_Init(GPIOD,GPIO_PIN_3,GPIO_MODE_OUT_PP_LOW_FAST);
  //GPIO_DeInit(GPIOC);
  //GPIO_Init(GPIOC,GPIO_PIN_4,GPIO_MODE_OUT_PP_LOW_FAST);
  GPIO_Init(GPIOC,GPIO_PIN_3,GPIO_MODE_IN_PU_IT);
  //    GPIO_Init(GPIOC,GPIO_PIN_2,GPIO_MODE_OUT_PP_LOW_FAST);
  
  //TIM1_DeInit();
  //TIM2_DeInit();
 // TIM2_ITConfig(TIM2_IT_UPDATE,ENABLE);
  /* Set TIM1 Frequency to 2Mhz and ARR=999 For output Freq of 2Khz*/ 
  TIM1_TimeBaseInit(32, TIM1_COUNTERMODE_UP,999,0);
  TIM2_TimeBaseInit(TIM2_PRESCALER_1024, 30000);
  /* TIM1 Channel 4 PWM configuration*/
  TIM2_ICInit(TIM2_CHANNEL_1, TIM2_ICPOLARITY_RISING,TIM2_ICSELECTION_DIRECTTI,TIM2_ICPSC_DIV1,0x00);
  /* TIM1 Channel4 duty cycle = TIM1_CCR4(250) / ( TIM1_Period(999) + 1) = 25%.*/
  TIM1_OC4Init(TIM1_OCMODE_PWM2, TIM1_OUTPUTSTATE_ENABLE,CCR4_Val, TIM1_OCPOLARITY_LOW,
               TIM1_OCIDLESTATE_SET);
  //enableInterrupts(); bad result
  __enable_interrupt();
 // UART1_DeInit();
  UART1_Init(115200, UART1_WORDLENGTH_8D, 
             UART1_STOPBITS_1, UART1_PARITY_NO, 
             UART1_SYNCMODE_CLOCK_DISABLE, UART1_MODE_TX_ENABLE);
  //  TIM1_SelectOnePulseMode(TIM1_OPMODE_SINGLE);
  UART1_Cmd(ENABLE);
  // TIM1->IER|=TIM1_IT_CC4;
  TIM1->IER|= TIM1_IT_UPDATE;
  TIM2->IER|=TIM2_IT_CC1;
  
  //     TIM1_CtrlPWMOutputs(ENABLE);
  //TIM2_ITConfig(TIM2_IT_UPDATE,ENABLE);
 TIM2_Cmd(ENABLE);
   TIM1_Cmd(ENABLE);
//  ADC1_Cmd(DISABLE);
 // EXTI_DeInit();
  EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOC , EXTI_SENSITIVITY_FALL_ONLY);
  
}
void creatmessage(void)
{
  calcKeySchedule(secretkey);
  calcCMACSubkey(cryptoblock);
  sharedblock.messagePayload.counterValue=seqcounter;
  sharedblock.messagePayload.serialNo=0x20202020;
  sharedblock.messagePayload.preamble=0xfe;
  
  
  //sharedblock.messagePayload.commandCode=command;
  
  //calcCMAC((const byte *)&sharedblock.messagePayload.serialNo,cryptoblock);
  
  
  
}
byte getcommand()
{
  byte cmd=0x00;
  rstaccess=FALSE;
  do{
    wfi();
    cmd=GPIO_ReadInputData(GPIOC);
  }while((~cmd)&BUTTON_PINS);
  rstaccess=TRUE;
  return cmd;
}
void usend(byte count,byte * data)
{
  for(byte i=0;i<count;i++)
  {
    while(!UART1_GetFlagStatus(UART1_FLAG_TC));
    UART1_SendData8(*data++);
    

  }
  
}
/**
* @brief Delay
* @param nCount
* @retval None
*/
void Delay(uint32_t nCount)
{
  /* Decrement nCount value */
  while (nCount != 0)
  {
    nCount--;
  }
}

#ifdef USE_FULL_ASSERT

/**
* @brief  Reports the name of the source file and the source line number
*   where the assert_param error has occurred.
* @param file: pointer to the source file name
* @param line: assert_param error line source number
* @retval None
*/
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
  ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  
  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
