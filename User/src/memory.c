// This file has been prepared for Doxygen automatic documentation generation.
/*! \file ********************************************************************
*
* \brief  Source file for EEPROM and SRAM variables and support functions.
*
* This file contains the variables in EEPROM and SRAM that er common to most
* of the application. Refer to the memory.h file for more details.
*
* - File:              memory.c
* - Compiler:          IAR EWAVR 4.11B
* - Supported devices: ATtiny45/85
* - AppNote:           AVR411 - Secure Rolling Code Algorithm
*                      for Wireless Link
*
* \author              Atmel Corporation: http://www.atmel.com \n
*                      Support email: avr@atmel.com
*
* $Name:  $
* $Revision: 1193 $
* $Date: 2006-10-31 14:21:08 +0100 (ti, 31 okt 2006) $
*
* Copyright (c) 2006, Atmel Corporation All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* 3. The name of ATMEL may not be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY ATMEL ``AS IS'' AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE EXPRESSLY AND
* SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT,
* INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
#include "memory.h"
#include "common.h"
#include "config.h"



/* --- EEPROM memory contents --- */

// Variables at absolute addresses are defined in the header file only.



/* --- SRAM memory contents --- */

byte cryptoBlock[ BLOCK_SIZE + 1 ];
union SharedBlock sharedBlock;



/* --- Associated functions --- */

void initializeMessage(void)
{
	// The preamble is used as a synch-and-start indicator for
	// the RF receiver.
	sharedBlock.messagePayload.preamble = 0xfe; // 11111110
	sharedBlock.messagePayload.serialNo = serialNo;
}



void createMessage( COMMAND_CODE_TYPE commandCode )
{
	sharedBlock.messagePayload.commandCode = commandCode;
	sharedBlock.messagePayload.counterValue = nextCounterValue;
}



//! Number of bytes left during update of counter value in EEPROM.
static volatile byte counterBytesLeft = 0;



void startCounterUpdate()
{
	// Make sure EEPROM is ready.
	do {} while( EECR & (1<<EEWE) );
	// Prepare control variables.
	EEDR = 0xff; // 0xff indicates that we just started.
	EEAR = (uint16_t) &nextCounterValue;
	counterBytesLeft = SEQ_COUNTER_BYTES;
	  // Enable EEPROM Ready Interrupt.
	EECR |= (1<<EERIE);
}



void waitForCounterUpdate()
{
	while( counterBytesLeft ) { __sleep(); }
}



/* --- ISR and variables for updating counter value --- */

//! Interrupt handler for EEPROM ready
#pragma vector = EE_RDY_vect
__interrupt void eepromReadyHandler(void)
{
	// If previous byte wrapped to 0x00, increase address.
	if( EEDR == 0x00 ) {
		++EEAR;
	}

	// Read old value.
	EECR |= (1<<EERE);
	// Increase.
	++EEDR;
	// If no overflow, no bytes left to update.
	if( EEDR != 0x00 ) {
		counterBytesLeft = 0;
		EECR &= ~(1<<EERIE); // Disable further interrupts.
	} else {
		--counterBytesLeft;
	}
	
	// Start writing.
	EECR |= (1<<EEMWE);
	EECR |= (1<<EEWE);
}

