// This file has been prepared for Doxygen automatic documentation generation.
/*! \file ********************************************************************
*
* \brief  Header file for EEPROM and SRAM variables and support functions.
*
* This file contains variable declarations and function prototypes for common
* data in EEPROM and SRAM. The implementation is found in the memory.c file.
* Note that the cryptographic key management PC tools relies upon the
* specific placement in EEPROM of the serialNo, nextCounterValue, secretKey
* and sharedKey variables.
*
* - File:              memory.h
* - Compiler:          IAR EWAVR 4.11B
* - Supported devices: ATtiny45/85
* - AppNote:           AVR411 - Secure Rolling Code Algorithm
*                      for Wireless Link
*
* \author              Atmel Corporation: http://www.atmel.com \n
*                      Support email: avr@atmel.com
*
* $Name:  $
* $Revision: 1193 $
* $Date: 2006-10-31 14:21:08 +0100 (ti, 31 okt 2006) $
*
* Copyright (c) 2006, Atmel Corporation All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* 3. The name of ATMEL may not be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY ATMEL ``AS IS'' AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE EXPRESSLY AND
* SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT,
* INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
#ifndef MEMORY_H
#define MEMORY_H

#include "common.h"
#include "config.h"



/* --- EEPROM memory contents --- */

// Due to placement at absolute addresses, these variables
// are defined in the header file without external declaration.
// This means that every translation unit will have its own
// copy of the variable, but since they occupy the same place in
// memory, it doesn't give any extra memory usage.

//! Serial number storage in EEPROM.
__no_init SERIAL_NO_TYPE __eeprom serialNo @ SERIAL_NO_ADDRESS;
//! Counter value storage in EEPROM.
__no_init SEQ_COUNTER_TYPE __eeprom nextCounterValue @ SEQ_COUNTER_ADDRESS;

//! Secret key storage in EEPROM.
__no_init byte __eeprom secretKey[ KEY_SIZE ] @ SECRET_KEY_ADDRESS;
//! Shared key storage in EEPROM.
__no_init byte __eeprom sharedKey[ KEY_SIZE ] @ SHARED_KEY_ADDRESS;



/* --- SRAM memory contents --- */

//! Common workspace for encrypting and decrypting.
extern byte cryptoBlock[ BLOCK_SIZE + 1 ]; // Leave room for one appended end byte when using this buffer to transmit MAC bytes.

//! Structure containing message payload without MAC.
struct MessagePayload
{
	byte preamble;
	SERIAL_NO_TYPE serialNo;
	SEQ_COUNTER_TYPE counterValue;
	COMMAND_CODE_TYPE commandCode;
};

union SharedBlock {
	struct MessagePayload messagePayload;
	byte cryptoBlock[ BLOCK_SIZE ];
};

//! Common workspace for message payloads and a second crypto block.
extern union SharedBlock sharedBlock;



/* --- Associated functions --- */



//! Prepare constant fields for message payload in 'sharedBlock'.
void initializeMessage(void);
//! Prepare non-constant fields for message payload in 'sharedBlock'.
void createMessage( COMMAND_CODE_TYPE commandCode );
//! Start interrupt controlled update of 'nextCounterValue' in EEPROM.
void startCounterUpdate();
//! Wait for counter value update to finish.
void waitForCounterUpdate();



#endif

