/**
  ******************************************************************************
  * @file    RS485.h
  * @brief   This file contains the header for RS485 Protocol.
  *****************************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __RS485_H
#define __RS485_H

#ifdef __cplusplus
 extern "C" {
#endif
   
/* Includes ------------------------------------------------------------------*/
#include "stm8s.h"
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
   
/*  
*                              +++++++++SEARCH_FOR_MATCH++++++++++++++
*Question From Master: 
*   START          ADDRESS         DATA_R          DATA_G          DATA_B          CRC_H           CRC_L           END
*    193(0xC1)          READ_PIN          ---             ---             ---                 All But CRC And End        234(0xEA)
*/

//Premble Codes For All Packages
#define RGB_STARTCODE                                   193
#define RGB_ENDCODE                                     234  

#define RGB_BROADCAST_ADDR_1                            255   
#define RGB_BROADCAST_ADDR_2                            0
#define RGB_DEVICE_ADDR                                 0x03

#define RGB_PACKLENGTH                                  8

#define RGB_TIMOUT                                      0xFF  
#define RGB_OK                                          0x00   
#define RGB_PACKERROR                                   0x02
   
#define RTU_UART_RECEIVE_TIMEOUT_MS                     100
#define UART_BUFF_SIZE 50   
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */   
uint8_t RS485_ReadPacketQuestion(uint8_t Packet[]);
   
#ifdef __cplusplus
}
#endif

#endif /* __RS485_H */