
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BSP_H
  #define __BSP_H
#include "stm8s.h"
#define READ_GPIO_PORT_1        (GPIOC)
#define WRITE_GPIO_PORT_1        (GPIOD)

#define READ_GPIO_UNLOCKKEY       (GPIO_PIN_3)
#define READ_GPIO_LOCKKEY         (GPIO_PIN_4)
#define READ_GPIO_MUTEKEY              (GPIO_PIN_5)
#define READ_GPIO_TRUNKKEY             (GPIO_PIN_7)
#define WRITE_RFCONFIGURATION     (GPIO_PIN_6)


#define         UNLOCK           (unsigned char)(1) /* UNLOCK flag */
#define         LOCK           (unsigned char)(2) /* LOCK flag */
#define         MUTE           (unsigned char)(3) /* MUTE flag */
#define         TRUNK           (unsigned char)(4) /* TRUNK flag */

#define         Duty_Cycle_Short           (uint16_t)(250) /* Short Pules */
#define         Duty_Cycle_Long           (uint16_t)(750) /* long Pules */
#define         Duty_Cycle_Sync           (uint16_t)(250) /* Short Pules for sync */



void UART_Config(void);
void GPIO_Config(void);
void EEPROM_Config(void);
void EEPROM_Write(uint16_t EEprom_ADD, uint8_t EEprom_buffer );
int EEPROM_Read(uint16_t EEprom_ADD, uint8_t *EEprom_buffer);
void TIM1_Config_1K(void);
void TIM1_Config_250(void);
void send_PT2262(uint8_t* PT2262_Buff, uint8_t Length_Buff);
void Active_timmer(uint16_t Duty_Cycle);
void send_RF_PT2262();
#endif /*__BSP_H*/