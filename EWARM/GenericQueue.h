/**
  ******************************************************************************
  * @file    GenericQueue.h
  * @author  Arshin Holding Developer Team
  * @version V1.0.0
  * @date    4-Sep-2015
  * @brief   Header file for Queue.c Library
  ******************************************************************************
	**/

#ifndef __GENERICQUEUE_H
#define __GENERICQUEUE_H


#ifdef __cplusplus
extern "C"
{
#endif

#define         EFULL           (unsigned char)(1) /* Queue full */
#define         EEMPTY          (unsigned char)(2) /* Queue empty */
#define         EALLOC          (unsigned char)(3) /* No memory allocated for queue */
#define         ENOMEM          (unsigned char)(4) /* No system memory */
#define         ENULL           (unsigned char)(5) /* NULL pointer */
#define         ENFREE          (unsigned char)(6) /* Queue already allocated */
#define         EINVAL          (unsigned char)(7) /* Invalid argument passed */

typedef struct 
{
    int tail;
    int head;
    int size;
    int count;
    unsigned char *data;
}GenericQueue_Type;


int GQueue_Init(GenericQueue_Type *q, int size, unsigned char *userBuf);
int GQueue_EmptyCheck(GenericQueue_Type *q);
int GQueue_FullCheck(GenericQueue_Type *q);
int GQueue_Add(GenericQueue_Type *q, unsigned char data);
int GQueue_Remove(GenericQueue_Type *q, unsigned char *data);
int GQueue_FrontRead(GenericQueue_Type *q, unsigned char *data);
int GQueue_BackRead(GenericQueue_Type *q, unsigned char *data);

#ifdef __cplusplus
}
#endif

#endif /* __GENERICQUEUE_H */

