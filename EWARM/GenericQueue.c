/**
  ******************************************************************************
  * @file    GenericQueue.c
  * @author  Arshin Holding Developer Team
  * @version V1.0.0
  * @date    4-Sep-2015
  * @brief   This file provides functions for circular buffer defination using in pheripherals 
  *          such as UART.
  ******************************************************************************
	**/

#include "GenericQueue.h"

/* Private Defenitions -------------------------------------------------------*/
#if !defined(NULL)
    #define NULL ((void*)0)
#endif


/* Public Functions ----------------------------------------------------------*/
/**
* @brief Init GenericQueue With User Defined Buffer
*/
int GQueue_Init(GenericQueue_Type *q, int size, unsigned char *userBuf)
{
  //reset queue if its defined.
    if (q) {
        q->tail  = 0;
        q->head  = 0;
        q->size  = 0;
        q->count = 0;
        q->data  = NULL;
    }
    else {
      return ENULL;
    }
    
    //check if user inserted size true or not
    if (size <= 0)
      return EINVAL;

    //queue is reffering to user buffer right now!
    q->data = userBuf;
    
    //if no user buffer, return error.
    if (q->data == NULL)
      return ENOMEM;
    
    //set size and go-back. 
    q->size = size;
      return 0;
}

/**
* @brief Checking Queue Status: For Empty Checking.
*/
int GQueue_EmptyCheck(GenericQueue_Type *q)
{
    return (q && q->count == 0);
}

/**
* @brief Checking Queue Status: For Full Checking.
*/
int GQueue_FullCheck(GenericQueue_Type *q)
{
    return (q && q->size == q->count);
}

/**
* @brief Writing To The Back of Queue.
*/

int GQueue_Add(GenericQueue_Type *q, unsigned char data)
{
    if (!q)
        return ENULL;

    if (q->data == NULL)
        return EALLOC;
 
    if (GQueue_FullCheck(q))
        return EFULL;

    q->data[q->head] =data;
    q->head = (q->head + 1) % q->size;
    q->count++;
    return 0;
}

/**
* @brief Reading From Front Of Queue (First Char Added to FIFO)
*/

int GQueue_Remove(GenericQueue_Type *q, unsigned char *data)
{
    if (!q || !data)
        return ENULL;
 
    if (q->data == NULL)
        return EALLOC;

    if (GQueue_EmptyCheck(q))
        return EEMPTY;

    *data = q->data[q->tail];
    q->tail = (q->tail + 1) % q->size;
    q->count--;

    return 0;
}

/**
* @brief Reading From Front Of Queue (First Char Added to FIFO)
* Without Removing From Queue. 
*/
int GQueue_FrontRead(GenericQueue_Type *q, unsigned char *data)
{
    if (!q || !data)
        return ENULL;

    if (GQueue_EmptyCheck(q))
        return EEMPTY;

    *data = q->data[q->tail];

    return 0;
}


/**
* @brief Reading From Back Of Queue (Last Char Added to FIFO)
* Without Removing From Queue. 
*/
int GQueue_BackRead(GenericQueue_Type *q, unsigned char *data)
{
    if (!q || !data)
        return ENULL;

    if (GQueue_EmptyCheck(q))
        return EEMPTY;

    if (q->head > 0)
        *data = q->data[q->head - 1];
    else
        *data = q->data[q->size - 1];

    return 0;
}
