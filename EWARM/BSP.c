
#include "BSP.h"
#include "stm8s.h"
static void Convert_Byteto_Bit(uint8_t Buffer_byte_send,uint8_t Buffer_bit_send[8]);
static void send_PT2262_bit(uint8_t PT_bit);
extern uint8_t Next_bit,start_send_data,Final,Buf[];
void GPIO_Config(void)
{
  GPIO_Init(READ_GPIO_PORT_1, (GPIO_Pin_TypeDef)READ_GPIO_UNLOCKKEY, GPIO_MODE_IN_PU_IT);
  GPIO_Init(READ_GPIO_PORT_1, (GPIO_Pin_TypeDef)READ_GPIO_LOCKKEY, GPIO_MODE_IN_PU_IT);
  GPIO_Init(READ_GPIO_PORT_1, (GPIO_Pin_TypeDef)READ_GPIO_MUTEKEY, GPIO_MODE_IN_PU_IT);
  GPIO_Init(READ_GPIO_PORT_1, (GPIO_Pin_TypeDef)READ_GPIO_TRUNKKEY, GPIO_MODE_IN_PU_IT);
  
  EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOC,EXTI_SENSITIVITY_FALL_ONLY);
   //EXTI_SetTLISensitivity(EXTI_TLISENSITIVITY_FALL_ONLY);
  GPIO_Init(WRITE_GPIO_PORT_1, (GPIO_Pin_TypeDef)WRITE_RFCONFIGURATION, GPIO_MODE_OUT_OD_LOW_SLOW);
  
  rim();
    
}

/**
  * @brief Config UART1 With 57600 8n1.
  * @par Parameters:
  * None
  * @retval 
  * None
  */
void UART_Config(void)
{
    UART1_DeInit();

   /* Configure the UART1 */
    UART1_Init((uint32_t)115200, UART1_WORDLENGTH_8D, UART1_STOPBITS_1, UART1_PARITY_NO,
                UART1_SYNCMODE_CLOCK_DISABLE, UART1_MODE_TXRX_ENABLE);
    
        
    UART1_Cmd(ENABLE);
    /* Enable UART1 Receive interrupt*/
   UART1_ITConfig(UART1_IT_RXNE, ENABLE);
    
   // 
    
    /* Enable general interrupts */  
    rim();        
}
void EEPROM_Config(void)
{
  
   /* Define FLASH programming time */
     FLASH_SetProgrammingTime(FLASH_PROGRAMTIME_STANDARD);

    /* Unlock Data memory */
    FLASH_Unlock(FLASH_MEMTYPE_DATA);

}
void EEPROM_Write(uint16_t EEprom_ADD, uint8_t EEprom_buffer)
{
  
  int add = 0x004000+EEprom_ADD;
   FLASH_ProgramByte(add, EEprom_buffer);
  return ;
}

int EEPROM_Read(uint16_t EEprom_ADD, uint8_t *EEprom_buffer)
{
  
  int add = 0x004000+EEprom_ADD;
   *EEprom_buffer=FLASH_ReadByte(add);
   return 0;
  }

void TIM1_Config_1K(void)
{
  
  /* TIM1 Peripheral Configuration */ 
  TIM1_DeInit();

  /* Time Base configuration for 1KHz with 16MHz FOSC*/
	/*
  TIM1_Prescaler = 16
  TIM1_CounterMode = TIM1_COUNTERMODE_UP
  TIM1_Period = 1000
  TIM1_RepetitionCounter = 0
	*/

  TIM1_TimeBaseInit(15, TIM1_COUNTERMODE_UP, 1000,0);
 
}
void TIM1_Config_250(void)
{
  /* TIM1 Peripheral Configuration */ 
  TIM1_DeInit();

  /* Time Base configuration for 1KHz with 16MHz FOSC*/
	/*
  TIM1_Prescaler = 16
  TIM1_CounterMode = TIM1_COUNTERMODE_UP
  TIM1_Period = 4000
  TIM1_RepetitionCounter = 0
	*/

  TIM1_TimeBaseInit(15, TIM1_COUNTERMODE_UP, 4000,0);
 
}


void Active_timmer(uint16_t Duty_Cycle)
{
 
  /* Channel 1 Configuration in PWM mode */
  /*
  TIM1_OCMode = TIM1_OCMODE_PWM2
  TIM1_OutputState = TIM1_OUTPUTSTATE_ENABLE
  TIM1_OutputNState = TIM1_OUTPUTNSTATE_DISABLE
  TIM1_Pulse = Duty_Cycle_Short || Duty_Cycle_Long || Duty_Cycle_Sync
  TIM1_OCPolarity = TIM1_OCPOLARITY_LOW 
  TIM1_OCNPolarity = TIM1_OCNPOLARITY_LOW        
  TIM1_OCIdleState = TIM1_OCIDLESTATE_SET
  TIM1_OCNIdleState = TIM1_OCIDLESTATE_RESET
	*/
  TIM1_OC1Init(TIM1_OCMODE_PWM2, TIM1_OUTPUTSTATE_ENABLE, TIM1_OUTPUTNSTATE_DISABLE,
               Duty_Cycle, TIM1_OCPOLARITY_LOW, TIM1_OCNPOLARITY_LOW, TIM1_OCIDLESTATE_SET,
               TIM1_OCNIDLESTATE_RESET);

  /* TIM1 counter enable */
  TIM1_Cmd(ENABLE);

  /* Main Output Enable */
  TIM1_CtrlPWMOutputs(ENABLE);
  
  TIM1_ITConfig(TIM1_IT_UPDATE,ENABLE);
    
  rim();        
}
  
void send_RF_PT2262()
{
  start_send_data=0;
  TIM1_InternalClockConfig;
  TIM1_Config_250();
  Active_timmer(Duty_Cycle_Sync);
  while(start_send_data)
  TIM1_Config_1K();
   UART1_SendData8('L');
  while(Final){
    UART1_SendData8('k');
    printf("Temum %d \n\r",Buf);
    Final=0;
  }
}

void send_PT2262(uint8_t* PT2262_Buff, uint8_t Length_Buff)
{
  uint8_t Buffer_bit_send[8]; // 1 byte
  for(uint8_t i=0; i<Length_Buff; i++)
  {
    Convert_Byteto_Bit(PT2262_Buff[i],Buffer_bit_send);
    
    for(uint8_t j=0; j<8; j++)
    {
    send_PT2262_bit(Buffer_bit_send[j]);
    while(Next_bit);  /*wait for send bit*/
    Next_bit=0;
    }
    
    
  }
  
  
  
}  
  
void Convert_Byteto_Bit(uint8_t Buffer_byte_send,uint8_t Buffer_bit_send[])
{
  int shift=1;
  for(uint8_t m=0; m<8 ;m++)
  {
 Buffer_bit_send[m]=Buffer_byte_send && shift;
  shift*=2;
  }
   
}

void send_PT2262_bit(uint8_t PT_bit)
{
  if(PT_bit==0)
  Active_timmer(Duty_Cycle_Short);
  if(PT_bit==1)
  Active_timmer(Duty_Cycle_Long);
}